import gym
import random
import numpy as np


env_name = "GuessingGame-v0"
env = gym.make(env_name)


class Agent:
    def __init__(self, env):
        self.a_l = env.action_space.low
        self.a_h = env.action_space.high
        self.a_s = env.action_space.shape

    def get_action(self, state):

        action = np.random.uniform(self.a_l, self.a_h, self.a_s)
        return action


agent = Agent(env)
state = env.reset()
for i in range(100):
    action = agent.get_action(state)
    state, reward, done, info = env.step(action)
